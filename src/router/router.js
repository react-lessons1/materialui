import { createBrowserRouter } from 'react-router-dom';

import { App } from 'src/components/App';
import { HomePage } from 'src/pages/HomePage';
import { NewsPage } from 'src/pages/NewsPage';
import { UserPage } from 'src/pages/UserPage';
import { NotfoundPage } from '../pages/NotfoundPage';
import { Modal2 } from 'src/components/Modal2';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <NotfoundPage />,
    children: [
      {
        element: <HomePage />,
        index: true,
      },
      {
        path: 'user',
        element: <UserPage />,
      },
      {
        path: 'news',
        element: <NewsPage />,
      },
      {
        path: 'modal',
        element: <Modal2 />,
      },
    ],
  },
]);
