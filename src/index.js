import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { router } from 'src/router/router.js';
import { ThemeProvider, createTheme } from '@mui/material';
import { RouterProvider } from 'react-router-dom';

const theme = createTheme({
  palette: {
    primary: {
      main: '#82dafb',
    },
    secondary: {
      main: '#da83bf',
    },
  },
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ThemeProvider theme={theme}>
    <RouterProvider router={router} />
  </ThemeProvider>
);
