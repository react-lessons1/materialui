import { Alert, IconButton, Snackbar } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

export const Snack = ({
  messageInfo,
  snackOpen,
  handleSnack = Function.prototype,
}) => {
  return (
    <Snackbar
      // severity="success" // for Alert message
      message={messageInfo ? messageInfo : undefined}
      open={snackOpen}
      onClose={handleSnack}
      autoHideDuration={5000}
      action={
        <IconButton
          aria-label="close"
          color="inherit"
          sx={{ p: 0.5 }}
          onClick={handleSnack}
        >
          <CloseIcon />
        </IconButton>
      }
    >
      {/* <Alert severity="warning">Item added in cart</Alert> */}
    </Snackbar>
  );
};
