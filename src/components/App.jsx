import {
  Grid,
  TextField,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from '@mui/material';
import { Container } from '@mui/system';
import '../styles/App.css';
import { Header } from './Header';
import { Basket } from './Basket';
import { Snack } from './Snack';
import { useState } from 'react';
import { NavLink, Outlet, Route, Routes, useLocation } from 'react-router-dom';

const order = [{ name: 'one' }, { name: 'two' }, { name: 'three' }];

export function App() {
  const [isCartOpen, setCartOpen] = useState(false);
  const [isSnackOpen, setSnackOpen] = useState(false);

  const location = useLocation();
  const background = location.state && location.state.background;

  return (
    <div className="App">
      {/* modal 2 */}
      <div className="2-modal-window">
        <NavLink to="/modal" state={{ background: location }}>
          Modal2
        </NavLink>
      </div>

      <Header
        orderLen={10}
        handleCart={() => setCartOpen(true)}
        handleSnack={() => setSnackOpen(true)}
      />
      <Basket
        order={order}
        cartOpen={isCartOpen}
        closeCart={() => setCartOpen(false)}
      />
      <Outlet />
      <Container>
        <TextField label="search" fullWidth sx={{ m: '10px 0' }} />
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 1, sm: 8, md: 12 }}
        >
          {Array.from(Array(6)).map((_, index) => (
            <Grid item xs={1} sm={4} md={4} key={index}>
              <Card
              // sx={{ maxWidth: 345 }}
              >
                <CardMedia
                  sx={{ height: 140, backgroundPosition: 'bottom' }}
                  image="https://weblinks.ru/wp-content/uploads/2022/02/Krasivye-kartinki-leta-na-zastavku-telefona-1.jpg"
                  title="green iguana"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    Lizard
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    Lizards are a widespread group of squamate reptiles, with
                    over 6,000 species, ranging across all continents except
                    Antarctica
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small">Share</Button>
                  <Button size="small">Learn More</Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
      <Snack
        snackOpen={isSnackOpen}
        handleSnack={() => setSnackOpen(false)}
        messageInfo="experimental message for snack module"
      />
    </div>
  );
}
