import { ShoppingBasket } from '@mui/icons-material';
import {
  Badge,
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
} from '@mui/material';

import style from '../styles/custom.module.css';
import styles from 'src/styles/Header.module.css';
import { useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';

export const Header = ({ handleCart, orderLen, handleSnack }) => {
  // 1 modal window
  const [open, setOpen] = useState(false);
  const handleOpenModal = () => {
    setOpen(true);
  };
  const handleCloseModal = () => {
    setOpen(false);
  };
  // 1 modal end

  // 2 modal window
  const location = useLocation();
  const [open2, setOpen2] = useState(false);
  const handleOpenModal2 = () => {
    setOpen2(true);
  };
  const handleCloseModal2 = () => {
    setOpen2(false);
  };
  // 2 modal end

  return (
    <AppBar position="static">
      <Toolbar>
        <nav className={styles.HeaderMenu}>
          <ul className={styles.HeaderMenu__list}>
            <li className={styles.HeaderMenu__item}>
              <NavLink to="/">Home</NavLink>
            </li>
            <li className={styles.HeaderMenu__item}>
              <NavLink to="/user">User</NavLink>
            </li>
            <li className={styles.HeaderMenu__item}>
              <NavLink to="/news">News</NavLink>
            </li>
          </ul>
        </nav>
        <div className={style.myButton}>
          <Button
            className={style.temp}
            onClick={handleSnack}
            color="secondary"
            variant="contained"
          >
            Snack
          </Button>
        </div>
        {/* modal 1 */}
        <div className="1-modal-window">
          <Button
            variant="outlined"
            onClick={handleOpenModal}
            color="secondary"
          >
            Modal1
          </Button>
          <Dialog
            open={open}
            onClose={handleCloseModal}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Title Modal window</DialogTitle>
            <DialogContent>
              <DialogContentText>Lorem ipsum dolor sit amet.</DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="userName"
                type="text"
                fullWidth
              />
              <TextField
                margin="dense"
                id="email"
                label="email"
                type="email"
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseModal} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <IconButton color="inherit" onClick={handleCart}>
          <Badge color="secondary" badgeContent={orderLen}>
            <ShoppingBasket />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};
