import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Button,
  DialogTitle,
  TextField,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

export const Modal2 = () => {
  const navigate = useNavigate();
  const handleCloseModal = () => {
    navigate(-1);
  };
  return (
    <Dialog
      open={true}
      onClose={handleCloseModal}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Title Modal window</DialogTitle>
      <DialogContent>
        <DialogContentText>Lorem ipsum dolor sit amet.</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="userName"
          type="text"
          fullWidth
        />
        <TextField
          margin="dense"
          id="email"
          label="email"
          type="email"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseModal} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};
