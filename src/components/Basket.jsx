import { ShoppingBasket } from '@mui/icons-material';
import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';

export const Basket = ({ order, cartOpen, closeCart = Function.prototype }) => {
  return (
    <Drawer
      anchor="right"
      open={cartOpen}
      onClose={closeCart}
      hideBackdrop={false}
      PaperProps={{ sx: { background: '#e1fffc' } }}
    >
      <List sx={{ width: '300px' }}>
        <ListItem>
          <ListItemIcon>
            <ShoppingBasket />
          </ListItemIcon>
          <ListItemText primary="Cards" />
        </ListItem>
        <Divider />
        {!order.length ? (
          <ListItem>Cart is empty</ListItem>
        ) : (
          order.map((item) => (
            <ListItem key={item.name}> {item.name} </ListItem>
          ))
        )}
      </List>
    </Drawer>
  );
};
